#!/usr/bin/env bash

function encode() {
	gtk-encode-symbolic-svg -o "share/icons/Diminye-Icons/status/$1/" "remote-icons/adwaita-icon-theme/Adwaita/scalable/status/$2" "$1x$1"

	convert "share/icons/Diminye-Icons/status/$1/$(echo $2 | sed 's/\.svg/.symbolic.png/')" +negate "share/icons/Diminye-Icons/status/$1/$(echo $2 | sed 's/\.svg/.symbolic.png/')"
}
function generateSets() {
	encode  16 $1
	encode  22 $1
	encode  24 $1
	encode  32 $1
	encode  48 $1
	encode 256 $1
	encode 512 $1
}

for f in $(ls remote-icons/adwaita-icon-theme/Adwaita/scalable/status/)
do
	generateSets "$f"
done
